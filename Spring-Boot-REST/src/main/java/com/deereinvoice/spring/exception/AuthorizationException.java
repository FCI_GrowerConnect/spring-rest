package com.deereinvoice.spring.exception;

public class AuthorizationException extends Exception{

    String errorMessage;
    public AuthorizationException(String msg){
        super(msg);
        errorMessage = msg;
    }

    @Override
    public String getMessage() {
        return this.errorMessage;
    }
}
