package com.deereinvoice.spring.exception;

public class DataNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private String errorMessage;

	public String getErrorMessage() {
		return errorMessage;
	}

	public DataNotFoundException(String exceptionMsg) {

		super(exceptionMsg);
		errorMessage = exceptionMsg;
	}

	public DataNotFoundException() {
		super();
	}
}
