package com.deereinvoice.spring.dto;

public class _lstRSCWH {

	public Integer getUserid() {
		return Userid;
	}

	public void setUserid(Integer userid) {
		Userid = userid;
	}

	public String getUserName() {
		return Username;
	}

	public void setUserName(String userName) {
		Username = userName;
	}

	public String getRacfid() {
		return Racfid;
	}

	public void setRacfid(String racfid) {
		Racfid = racfid;
	}

	public String getComments() {
		return Comments;
	}

	public void setComments(String comments) {
		Comments = comments;
	}

	public Integer getChargemonthid() {
		return chargemonthid;
	}

	public void setChargemonthid(Integer chargemonthid) {
		this.chargemonthid = chargemonthid;
	}

	public Integer getWeek1() {
		return Week1;
	}

	public void setWeek1(Integer week1) {
		Week1 = week1;
	}

	public Integer getWeek2() {
		return Week2;
	}

	public void setWeek2(Integer week2) {
		Week2 = week2;
	}

	public Integer getWeek3() {
		return Week3;
	}

	public void setWeek3(Integer week3) {
		Week3 = week3;
	}

	public Integer getWeek4() {
		return Week4;
	}

	public void setWeek4(Integer week4) {
		Week4 = week4;
	}

	public Integer getWeek5() {
		return Week5;
	}

	public void setWeek5(Integer week5) {
		Week5 = week5;
	}

	public Integer getWeek6() {
		return Week6;
	}

	public void setWeek6(Integer week6) {
		Week6 = week6;
	}

	Integer Userid;

	public _lstRSCWH(Integer userid, String username, String racfid, Integer chargemonthid, Integer week1,
					 Integer week2, Integer week3, Integer week4, Integer week5, Integer week6, String comments,Integer rate) {
		Userid = userid;
		Username = username;
		Racfid = racfid;
		this.chargemonthid = chargemonthid;
		Week1 = week1;
		Week2 = week2;
		Week3 = week3;
		Week4 = week4;
		Week5 = week5;
		Week6 = week6;
		Comments = comments;
		this.rate = rate;
	}

	private String Username;
	private String Racfid;
	private String Comments;
	private Integer chargemonthid;
	private Integer Week1;
	private Integer Week2;
	private Integer Week3;
	private Integer Week4;
	private Integer Week5;
	private Integer Week6;

	public Integer getRate() {
		return rate;
	}

	public void setRate(Integer rate) {
		this.rate = rate;
	}

	private Integer rate;

}
