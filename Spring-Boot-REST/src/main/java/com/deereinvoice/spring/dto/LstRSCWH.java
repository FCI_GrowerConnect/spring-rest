
package com.deereinvoice.spring.dto;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "userId", "username", "chargeMonthId", "week1", "week2", "week3", "week4", "week5", "week6",
		"totalHours", "ctsId", "racfid", "name", "costCenter", "projectId", "projectName", "projectManager", "rate",
		"comments", "poNumber","totalAmount" })
public class LstRSCWH {
	@JsonProperty("totalAmount")
	public Integer getTotalAmount() {
		return totalAmount;
	}
	@JsonProperty("totalAmount")
	public void setTotalAmount(Integer totalAmount) {
		this.totalAmount = totalAmount;
	}

	@JsonProperty("totalAmount")
	private Integer totalAmount;
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("userId")
	private Integer userId;
	@JsonProperty("username")
	private String username;
	@JsonProperty("chargeMonthId")
	private Integer chargeMonthId;
	@JsonProperty("week1")
	private Integer week1;
	@JsonProperty("week2")
	private Integer week2;
	@JsonProperty("week3")
	private Integer week3;
	@JsonProperty("week4")
	private Integer week4;
	@JsonProperty("week5")
	private Integer week5;
	@JsonProperty("week6")
	private Integer week6;
	@JsonProperty("totalHours")
	private Integer totalHours;
	@JsonProperty("ctsId")
	private Integer ctsId;
	@JsonProperty("racfid")
	private String racfid;
	@JsonProperty("name")
	private Object name;
	@JsonProperty("costCenter")
	private Object costCenter;
	@JsonProperty("projectId")
	private Object projectId;
	@JsonProperty("projectName")
	private Object projectName;
	@JsonProperty("projectManager")
	private Object projectManager;
	@JsonProperty("rate")
	private Integer rate;
	@JsonProperty("comments")
	private String comments;
	@JsonProperty("poNumber")
	private Object poNumber;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("userId")
	public Integer getUserId() {
		return userId;
	}

	@JsonProperty("userId")
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@JsonProperty("username")
	public String getUsername() {
		return username;
	}

	@JsonProperty("username")
	public void setUsername(String username) {
		this.username = username;
	}

	@JsonProperty("chargeMonthId")
	public Integer getChargeMonthId() {
		return chargeMonthId;
	}

	@JsonProperty("chargeMonthId")
	public void setChargeMonthId(Integer chargeMonthId) {
		this.chargeMonthId = chargeMonthId;
	}

	@JsonProperty("week1")
	public Integer getWeek1() {
		return week1;
	}

	@JsonProperty("week1")
	public void setWeek1(Integer week1) {
		this.week1 = week1;
	}

	@JsonProperty("week2")
	public Integer getWeek2() {
		return week2;
	}

	@JsonProperty("week2")
	public void setWeek2(Integer week2) {
		this.week2 = week2;
	}

	@JsonProperty("week3")
	public Integer getWeek3() {
		return week3;
	}

	@JsonProperty("week3")
	public void setWeek3(Integer week3) {
		this.week3 = week3;
	}

	@JsonProperty("week4")
	public Integer getWeek4() {
		return week4;
	}

	@JsonProperty("week4")
	public void setWeek4(Integer week4) {
		this.week4 = week4;
	}

	@JsonProperty("week5")
	public Integer getWeek5() {
		return week5;
	}

	@JsonProperty("week5")
	public void setWeek5(Integer week5) {
		this.week5 = week5;
	}

	@JsonProperty("week6")
	public Integer getWeek6() {
		return week6;
	}

	@JsonProperty("week6")
	public void setWeek6(Integer week6) {
		this.week6 = week6;
	}

	@JsonProperty("totalHours")
	public Integer getTotalHours() {
		return totalHours;
	}

	@JsonProperty("totalHours")
	public void setTotalHours(Integer totalHours) {
		this.totalHours = totalHours;
	}

	@JsonProperty("ctsId")
	public Integer getCtsId() {
		return ctsId;
	}

	@JsonProperty("ctsId")
	public void setCtsId(Integer ctsId) {
		this.ctsId = ctsId;
	}

	@JsonProperty("racfid")
	public String getRacfid() {
		return racfid;
	}

	@JsonProperty("racfid")
	public void setRacfid(String racfid) {
		this.racfid = racfid;
	}

	@JsonProperty("name")
	public Object getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(Object name) {
		this.name = name;
	}

	@JsonProperty("costCenter")
	public Object getCostCenter() {
		return costCenter;
	}

	@JsonProperty("costCenter")
	public void setCostCenter(Object costCenter) {
		this.costCenter = costCenter;
	}

	@JsonProperty("projectId")
	public Object getProjectId() {
		return projectId;
	}

	@JsonProperty("projectId")
	public void setProjectId(Object projectId) {
		this.projectId = projectId;
	}

	@JsonProperty("projectName")
	public Object getProjectName() {
		return projectName;
	}

	@JsonProperty("projectName")
	public void setProjectName(Object projectName) {
		this.projectName = projectName;
	}

	@JsonProperty("projectManager")
	public Object getProjectManager() {
		return projectManager;
	}

	@JsonProperty("projectManager")
	public void setProjectManager(Object projectManager) {
		this.projectManager = projectManager;
	}

	@JsonProperty("rate")
	public Integer getRate() {
		return rate;
	}

	@JsonProperty("rate")
	public void setRate(Integer rate) {
		this.rate = rate;
	}

	@JsonProperty("comments")
	public String getComments() {
		return comments;
	}

	@JsonProperty("comments")
	public void setComments(String comments) {
		this.comments = comments;
	}

	@JsonProperty("poNumber")
	public Object getPoNumber() {
		return poNumber;
	}

	@JsonProperty("poNumber")
	public void setPoNumber(Object poNumber) {
		this.poNumber = poNumber;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
