package com.deereinvoice.spring.dto;

import org.springframework.stereotype.Component;

@Component("LoginResponse")
public class LoginResponse {

    String _city;
    String _company;
    String _country;
    String _department;
    String _emailAddress;
    String _extension;
    String _fax;
    String _firstName;
    String _homePhone;
    String _lastName;
    String _loginName;
    String _loginNameWithDomain;
    String _manager;
    String _managerName;
    String _middleName;
    Integer _mobile;
    Integer _postalCode;
    String _state;
    String _streetAddress;
    String _title;

    public String get_city() {
        return _city;
    }

    public void set_city(String _city) {
        this._city = _city;
    }

    public String get_company() {
        return _company;
    }

    public void set_company(String _company) {
        this._company = _company;
    }

    public String get_country() {
        return _country;
    }

    public void set_country(String _country) {
        this._country = _country;
    }

    public String get_department() {
        return _department;
    }

    public void set_department(String _department) {
        this._department = _department;
    }

    public String get_emailAddress() {
        return _emailAddress;
    }

    public void set_emailAddress(String _emailAddress) {
        this._emailAddress = _emailAddress;
    }

    public String get_extension() {
        return _extension;
    }

    public void set_extension(String _extension) {
        this._extension = _extension;
    }

    public String get_fax() {
        return _fax;
    }

    public void set_fax(String _fax) {
        this._fax = _fax;
    }

    public String get_firstName() {
        return _firstName;
    }

    public void set_firstName(String _firstName) {
        this._firstName = _firstName;
    }

    public String get_homePhone() {
        return _homePhone;
    }

    public void set_homePhone(String _homePhone) {
        this._homePhone = _homePhone;
    }

    public String get_lastName() {
        return _lastName;
    }

    public void set_lastName(String _lastName) {
        this._lastName = _lastName;
    }

    public String get_loginName() {
        return _loginName;
    }

    public void set_loginName(String _loginName) {
        this._loginName = _loginName;
    }

    public String get_loginNameWithDomain() {
        return _loginNameWithDomain;
    }

    public void set_loginNameWithDomain(String _loginNameWithDomain) {
        this._loginNameWithDomain = _loginNameWithDomain;
    }

    public String get_manager() {
        return _manager;
    }

    public void set_manager(String _manager) {
        this._manager = _manager;
    }

    public String get_managerName() {
        return _managerName;
    }

    public void set_managerName(String _managerName) {
        this._managerName = _managerName;
    }

    public String get_middleName() {
        return _middleName;
    }

    public void set_middleName(String _middleName) {
        this._middleName = _middleName;
    }

    public Integer get_mobile() {
        return _mobile;
    }

    public void set_mobile(Integer _mobile) {
        this._mobile = _mobile;
    }

    public Integer get_postalCode() {
        return _postalCode;
    }

    public void set_postalCode(Integer _postalCode) {
        this._postalCode = _postalCode;
    }

    public String get_state() {
        return _state;
    }

    public void set_state(String _state) {
        this._state = _state;
    }

    public String get_streetAddress() {
        return _streetAddress;
    }

    public void set_streetAddress(String _streetAddress) {
        this._streetAddress = _streetAddress;
    }

    public String get_title() {
        return _title;
    }

    public void set_title(String _title) {
        this._title = _title;
    }

    public String get_token() {
        return _token;
    }

    public void set_token(String _token) {
        this._token = _token;
    }

    public Integer get_userId() {
        return _userId;
    }

    public void set_userId(Integer _userId) {
        this._userId = _userId;
    }

    public Integer get_userRole() {
        return _userRole;
    }

    public void set_userRole(Integer _userRole) {
        this._userRole = _userRole;
    }

    String _token;
    Integer _userId;
    Integer _userRole;
}
