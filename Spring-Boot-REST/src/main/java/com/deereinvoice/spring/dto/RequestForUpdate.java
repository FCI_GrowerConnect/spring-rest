package com.deereinvoice.spring.dto;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"chargeMonthId", "userId", "username", "week1", "week2", "week3", "week4", "week5", "week6", "comments"})
public class RequestForUpdate {

    @JsonProperty("chargeMonthId")
    private Integer chargeMonthId;
    @JsonProperty("rate")
    private Integer rate;
    @JsonProperty("userId")
    private Integer userId;
    @JsonProperty("username")
    private String username;
    @JsonProperty("week1")
    private Integer week1;
    @JsonProperty("week2")
    private Integer week2;
    @JsonProperty("week3")
    private Integer week3;
    @JsonProperty("week4")
    private Integer week4;
    @JsonProperty("week5")
    private Integer week5;
    @JsonProperty("week6")
    private Integer week6;
    @JsonProperty("comments")
    private String comments;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("rate")
    public Integer getRate() {
        return rate;
    }

    @JsonProperty("rate")
    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @JsonProperty("chargeMonthID")
    public Integer getChargeMonthId() {
        return chargeMonthId;
    }

    @JsonProperty("chargeMonthID")
    public void setChargeMonthId(Integer chargeMonthId) {
        this.chargeMonthId = chargeMonthId;
    }

    @JsonProperty("userId")
    public Integer getUserId() {
        return userId;
    }

    @JsonProperty("userId")
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @JsonProperty("username")
    public String getUsername() {
        return username;
    }

    @JsonProperty("username")
    public void setUsername(String username) {
        this.username = username;
    }

    @JsonProperty("week1")
    public Integer getWeek1() {
        return week1;
    }

    @JsonProperty("week1")
    public void setWeek1(Integer week1) {
        this.week1 = week1;
    }

    @JsonProperty("week2")
    public Integer getWeek2() {
        return week2;
    }

    @JsonProperty("week2")
    public void setWeek2(Integer week2) {
        this.week2 = week2;
    }

    @JsonProperty("week3")
    public Integer getWeek3() {
        return week3;
    }

    @JsonProperty("week3")
    public void setWeek3(Integer week3) {
        this.week3 = week3;
    }

    @JsonProperty("week4")
    public Integer getWeek4() {
        return week4;
    }

    @JsonProperty("week4")
    public void setWeek4(Integer week4) {
        this.week4 = week4;
    }

    @JsonProperty("week5")
    public Integer getWeek5() {
        return week5;
    }

    @JsonProperty("week5")
    public void setWeek5(Integer week5) {
        this.week5 = week5;
    }

    @JsonProperty("week6")
    public Integer getWeek6() {
        return week6;
    }

    @JsonProperty("week6")
    public void setWeek6(Integer week6) {
        this.week6 = week6;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }


    @Override
    public String toString() {
        return "chargeMonthId=" + this.chargeMonthId + " "
                + "rate=" + this.rate + " "
                + "userId=" + this.userId + " "
                + "username=" + this.username
                + "week1=" + this.week1 + " "
                + "week2=" + this.week2 + " "
                + "week3=" + this.week3 + " "
                + "week4=" + this.week4 + " "
                + "week5=" + this.week5 + " "
                + "week6=" + this.week6 + " "
                + "comments=" + this.comments;
    }
}