package com.deereinvoice.spring.dto;

public class RequestDTO {

    String chargeYear;
    String userRole;
    String userID;
    String chargeMonth;

    public String getChargeMonth() {
        return chargeMonth;
    }

    public void setChargeMonth(String chargeMonth) {
        this.chargeMonth = chargeMonth;
    }

    public String getChargeYear() {
        return chargeYear;
    }

    public void setChargeYear(String chargeYear) {
        this.chargeYear = chargeYear;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    @Override
    public String toString() {
        return "ChargeYear=" + this.chargeYear + " "
                + "UserRole=" + this.userRole + " "
                + "UserID=" + this.userID + " "
                + "ChargeMonth=" + this.chargeMonth;
    }
}
