
package com.deereinvoice.spring.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Component("result")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "chargeMonthID", "chargeMonth", "chargeYear", "billingFrom", "billingTo", "_lstRSCWH",
		"_lstESARSCWH" })
public class Result {

	@JsonProperty("chargeMonthID")
	private Integer chargeMonthID;
	@JsonProperty("chargeMonth")
	private String chargeMonth;
	@JsonProperty("chargeYear")
	private Integer chargeYear;
	@JsonProperty("billingFrom")
	private String billingFrom;
	@JsonProperty("billingTo")
	private String billingTo;
	@JsonProperty("_lstRSCWH")
	private List<LstRSCWH> lstRSCWH = null;
	@JsonProperty("_lstESARSCWH")
	private List<LstESARSCWH> lstESARSCWH = null;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("chargeMonthID")
	public Integer getChargeMonthID() {
		return chargeMonthID;
	}

	@JsonProperty("chargeMonthID")
	public void setChargeMonthID(Integer chargeMonthID) {
		this.chargeMonthID = chargeMonthID;
	}

	@JsonProperty("chargeMonth")
	public String getChargeMonth() {
		return chargeMonth;
	}

	@JsonProperty("chargeMonth")
	public void setChargeMonth(String chargeMonth) {
		this.chargeMonth = chargeMonth;
	}

	@JsonProperty("chargeYear")
	public Integer getChargeYear() {
		return chargeYear;
	}

	@JsonProperty("chargeYear")
	public void setChargeYear(Integer chargeYear) {
		this.chargeYear = chargeYear;
	}

	@JsonProperty("billingFrom")
	public String getBillingFrom() {
		return billingFrom;
	}

	@JsonProperty("billingFrom")
	public void setBillingFrom(String billingFrom) {
		this.billingFrom = billingFrom;
	}

	@JsonProperty("billingTo")
	public String getBillingTo() {
		return billingTo;
	}

	@JsonProperty("billingTo")
	public void setBillingTo(String billingTo) {
		this.billingTo = billingTo;
	}

	@JsonProperty("_lstRSCWH")
	public List<LstRSCWH> getLstRSCWH() {
		return lstRSCWH;
	}

	@JsonProperty("_lstRSCWH")
	public void setLstRSCWH(List<LstRSCWH> lstRSCWH) {
		this.lstRSCWH = lstRSCWH;
	}

	@JsonProperty("_lstESARSCWH")
	public List<LstESARSCWH> getLstESARSCWH() {
		return lstESARSCWH;
	}

	@JsonProperty("_lstESARSCWH")
	public void setLstESARSCWH(List<LstESARSCWH> lstESARSCWH) {
		this.lstESARSCWH = lstESARSCWH;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
