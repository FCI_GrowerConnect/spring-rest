package com.deereinvoice.spring.dto;


import org.springframework.stereotype.Component;

@Component("loginResult")
public class LoginResult {
    public LoginResponse getResult() {
        return result;
    }

    public void setResult(LoginResponse result) {
        this.result = result;
    }

    LoginResponse result = null;
}
