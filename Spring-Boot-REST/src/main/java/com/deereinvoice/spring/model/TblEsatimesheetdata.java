package com.deereinvoice.spring.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class TblEsatimesheetdata {

	private Date periodEndDate;

	public Date getPeriodEndDate() {
		return periodEndDate;
	}

	public void setPeriodEndDate(Date periodEndDate) {
		this.periodEndDate = periodEndDate;
	}

	public Integer getFkUserid() {
		return fkUserid;
	}

	public void setFkUserid(Integer fkUserid) {
		this.fkUserid = fkUserid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getFkProjectIid() {
		return fkProjectIid;
	}

	public void setFkProjectIid(Integer fkProjectIid) {
		this.fkProjectIid = fkProjectIid;
	}

	public Integer getSupervisorid() {
		return supervisorid;
	}

	public void setSupervisorid(Integer supervisorid) {
		this.supervisorid = supervisorid;
	}

	public Integer getProjectManagerid() {
		return projectManagerid;
	}

	public void setProjectManagerid(Integer projectManagerid) {
		this.projectManagerid = projectManagerid;
	}

	public String getCtsRmOffOnsite() {
		return ctsRmOffOnsite;
	}

	public void setCtsRmOffOnsite(String ctsRmOffOnsite) {
		this.ctsRmOffOnsite = ctsRmOffOnsite;
	}

	public String getCtsRmBillability() {
		return ctsRmBillability;
	}

	public void setCtsRmBillability(String ctsRmBillability) {
		this.ctsRmBillability = ctsRmBillability;
	}

	public String getPercentage() {
		return percentage;
	}

	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}

	public String getTimesheetstatus() {
		return timesheetstatus;
	}

	public void setTimesheetstatus(String timesheetstatus) {
		this.timesheetstatus = timesheetstatus;
	}

	public Double getDtlTotal() {
		return dtlTotal;
	}

	public void setDtlTotal(Double dtlTotal) {
		this.dtlTotal = dtlTotal;
	}

	public Double getTimeQuantity() {
		return timeQuantity;
	}

	public void setTimeQuantity(Double timeQuantity) {
		this.timeQuantity = timeQuantity;
	}

	public Date getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}

	public Date getModifiedon() {
		return modifiedon;
	}

	public void setModifiedon(Date modifiedon) {
		this.modifiedon = modifiedon;
	}

	@Id
	private Integer fkUserid;
	private String username;
	private Integer fkProjectIid;
	private Integer supervisorid;
	private Integer projectManagerid;
	private String ctsRmOffOnsite;
	private String ctsRmBillability;
	private String percentage;
	private String timesheetstatus;
	private Double dtlTotal;
	private Double timeQuantity;
	private Date createdon;
	private Date modifiedon;
}
