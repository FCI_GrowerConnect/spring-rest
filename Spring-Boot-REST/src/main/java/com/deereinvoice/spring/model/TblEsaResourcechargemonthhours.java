package com.deereinvoice.spring.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import io.swagger.annotations.ApiModel;

@Entity
@ApiModel(description = "Resource charge months details from Cognizant ESA ")
public class TblEsaResourcechargemonthhours {
	public int getSrid() {
		return srid;
	}

	public void setSrid(int srid) {
		this.srid = srid;
	}

	public int getFkUserid() {
		return fkUserid;
	}

	public void setFkUserid(int fkUserid) {
		this.fkUserid = fkUserid;
	}

	public int getFkChargemonthid() {
		return fkChargemonthid;
	}

	public void setFkChargemonthid(int fkChargemonthid) {
		this.fkChargemonthid = fkChargemonthid;
	}

	public int getWeek1() {
		return week1;
	}

	public void setWeek1(int week1) {
		this.week1 = week1;
	}

	public int getWeek2() {
		return week2;
	}

	public void setWeek2(int week2) {
		this.week2 = week2;
	}

	public int getWeek3() {
		return week3;
	}

	public void setWeek3(int week3) {
		this.week3 = week3;
	}

	public int getWeek4() {
		return week4;
	}

	public void setWeek4(int week4) {
		this.week4 = week4;
	}

	public int getWeek5() {
		return week5;
	}

	public void setWeek5(int week5) {
		this.week5 = week5;
	}

	public int getWeek6() {
		return week6;
	}

	public void setWeek6(int week6) {
		this.week6 = week6;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Id
	int srid;
	int fkUserid;
	int fkChargemonthid;
	int week1;
	int week2;
	int week3;
	int week4;
	int week5;
	int week6;
	int rate;

	public int getRate() {
		return rate;
	}

	public void setRate(int rate) {
		this.rate = rate;
	}

	String comments;
}