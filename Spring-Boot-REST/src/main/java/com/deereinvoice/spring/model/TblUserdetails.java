package com.deereinvoice.spring.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

import io.swagger.annotations.ApiModel;

@Entity
@ApiModel(description = "All details about the User Details. ")
public class TblUserdetails {

	@Id
	int srid;

	public int getSrid() {
		return srid;
	}

	public void setSrid(int srid) {
		this.srid = srid;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getfRoleid() {
		return fRoleid;
	}

	public void setfRoleid(int fRoleid) {
		this.fRoleid = fRoleid;
	}

	public String getRacfid() {
		return racfid;
	}

	public void setRacfid(String racfid) {
		this.racfid = racfid;
	}

	public boolean isIsctsuser() {
		return isctsuser;
	}

	public void setIsctsuser(boolean isctsuser) {
		this.isctsuser = isctsuser;
	}

	public Date getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}

	public Date getModifiedon() {
		return modifiedon;
	}

	public void setModifiedon(Date modifiedon) {
		this.modifiedon = modifiedon;
	}

	Integer userid;
	String username;
	Integer fRoleid;
	String racfid;
	boolean isctsuser;
	Date createdon;
	Date modifiedon;
}
