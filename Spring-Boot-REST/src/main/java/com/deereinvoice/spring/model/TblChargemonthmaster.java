package com.deereinvoice.spring.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
public class TblChargemonthmaster {
	@Id
	int chargemonthid;

	public int getchargemonthid() {
		return chargemonthid;
	}

	public void setchargemonthid(int chargemonthid) {
		this.chargemonthid = chargemonthid;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public Date getfromdate() {
		return fromdate;
	}

	public void setfromdate(Date fromdate) {
		this.fromdate = fromdate;
	}

	public Date gettodate() {
		return todate;
	}

	public void settodate(Date todate) {
		this.todate = todate;
	}

	public String getchargemonth() {
		return chargemonth;
	}

	public void setchargemonth(String chargemonth) {
		this.chargemonth = chargemonth;
	}

	int year;
	Date fromdate;
	Date todate;
	String chargemonth;

}
