package com.deereinvoice.spring.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deereinvoice.spring.model.TblChargemonthmaster;
import com.deereinvoice.spring.repository.ChargeMonthMasterRepository;

@Service
public class ChargeMonthMasterService {

	@Autowired
	public ChargeMonthMasterRepository chargeMonthMasterRepository;

	public List<TblChargemonthmaster> getAllUsers() {
		List<TblChargemonthmaster> userRecords = new ArrayList<>();
		chargeMonthMasterRepository.findAll().forEach(userRecords::add);
		return userRecords;
	}

	public Optional<TblChargemonthmaster> getUser(Integer id) {
		return chargeMonthMasterRepository.findById(id);
	}

	public void addUser(TblChargemonthmaster userRecord) {
		chargeMonthMasterRepository.save(userRecord);
	}

	public void delete(Integer id) {
		chargeMonthMasterRepository.deleteById(id);
	}

	public TblChargemonthmaster findByChargeMonthAndYear(String month, int year) {
		return chargeMonthMasterRepository.findByChargeMonthAndYear(month, year);
	}

	public TblChargemonthmaster findByChargeMonthForCurrentMonth() {
		return chargeMonthMasterRepository.findByChargeMonthForCurrentMonth();
	}
}
