package com.deereinvoice.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deereinvoice.spring.dto._lstRSCWH;
import com.deereinvoice.spring.repository.TblEsaResourcechargemonthhoursRepository;

@Service
public class TblEsaResourcechargemonthhoursService {

	@Autowired
	public TblEsaResourcechargemonthhoursRepository tblEsaResourcechargemonthhoursRepository;

	public List<_lstRSCWH> find_lstRSCWH(Integer chargemonthid) {
		return tblEsaResourcechargemonthhoursRepository.find_lstRSCWH(chargemonthid);
	}

	public List<_lstRSCWH> find_lstRSCWHForUser(Integer chargemonthid, Integer userid) {
		return tblEsaResourcechargemonthhoursRepository.find_lstRSCWHForUser(chargemonthid, userid);
	}

    public List<_lstRSCWH> find_lstRSCWHForDeereUser(int chargemonthid, Integer userid) {
		return tblEsaResourcechargemonthhoursRepository.find_lstRSCWHForDeereUser(chargemonthid, userid);
    }
}
