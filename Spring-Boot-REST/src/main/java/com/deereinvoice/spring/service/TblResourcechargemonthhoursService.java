package com.deereinvoice.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deereinvoice.spring.dto._lstRSCWH;
import com.deereinvoice.spring.model.TblResourcechargemonthhours;
import com.deereinvoice.spring.repository.TblResourcechargemonthhoursRepository;

@Service
public class TblResourcechargemonthhoursService {

	@Autowired
	public TblResourcechargemonthhoursRepository tblResourcechargemonthhoursRepository;

	public List<_lstRSCWH> find_lstRSCWH(Integer chargemonthid) {
		return tblResourcechargemonthhoursRepository.find_lstRSCWH(chargemonthid);
	}

	public List<_lstRSCWH> find_lstRSCWHForUser(Integer chargemonthid, Integer userid) {
		return tblResourcechargemonthhoursRepository.find_lstRSCWHForUser(chargemonthid, userid);
	}

	public Integer find_countForUserAndChargeMonthId(Integer chargemonthid, Integer userid) {
		return tblResourcechargemonthhoursRepository.find_countForUserAndChargeMonthId(chargemonthid, userid);
	}

	public Integer find_maxSrId() {
		return tblResourcechargemonthhoursRepository.find_maxSrId();
	}

	public void updateTimesheetData(Integer userid,Integer rate, Integer chargemonthid, Integer week1, Integer week2, Integer week3,
			Integer week4, Integer week5, Integer week6, String comments) {
		tblResourcechargemonthhoursRepository.updateTimesheetData(userid,rate, chargemonthid, week1, week2, week3, week4,
				week5, week6, comments);
	}

	public void save(TblResourcechargemonthhours tblResourcechargemonthhours) {
		tblResourcechargemonthhoursRepository.save(tblResourcechargemonthhours);
	}

	public List<_lstRSCWH> find_lstRSCWHForDeereUser(Integer chargemonthid,Integer userid) {
		return tblResourcechargemonthhoursRepository.find_lstRSCWHFOrDeereuser(chargemonthid,userid);
	}




}
