package com.deereinvoice.spring.service;


import com.deereinvoice.spring.model.TblChargemonthmaster;
import com.deereinvoice.spring.model.TblUserdetails;
import com.deereinvoice.spring.repository.UserDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDetailsService {


    @Autowired
    UserDetailsRepository userDetailsRepository;

    public TblUserdetails getUserByUserId(Integer id) {
        return userDetailsRepository.findByUserId(id);
    }
}
