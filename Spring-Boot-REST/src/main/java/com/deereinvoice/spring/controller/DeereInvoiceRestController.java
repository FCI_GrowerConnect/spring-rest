package com.deereinvoice.spring.controller;

import com.deereinvoice.spring.dto.*;
import com.deereinvoice.spring.exception.AuthorizationException;
import com.deereinvoice.spring.exception.DataNotFoundException;
import com.deereinvoice.spring.exception.ErrorResponse;
import com.deereinvoice.spring.model.TblChargemonthmaster;
import com.deereinvoice.spring.model.TblResourcechargemonthhours;
import com.deereinvoice.spring.service.ChargeMonthMasterService;
import com.deereinvoice.spring.service.TblEsaResourcechargemonthhoursService;
import com.deereinvoice.spring.service.TblResourcechargemonthhoursService;
import com.deereinvoice.spring.service.UserDetailsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/chargemnthhrs")
@Api(value = "Deere Invoice Management System", description = "Operations for managing Deere Invoice")
public class DeereInvoiceRestController {

    Logger logger = LoggerFactory.getLogger(DeereInvoiceRestController.class);

    @Autowired
    ChargeMonthMasterService chargeMonthMasterService;

    @Autowired
    TblResourcechargemonthhoursService tblResourcechargemonthhoursService;

    @Autowired
    TblEsaResourcechargemonthhoursService tblEsaResourcechargemonthhoursService;

    @Autowired
    UserDetailsService userDetailsService;

    @Autowired
    Response response;

    @Autowired
    Result result;

    private TblChargemonthmaster chargeMonthMaster;

    @CrossOrigin
    @ApiOperation(value = "GetResourceWorkDetails fetch deatils of timesheet data")
    @RequestMapping(value = "/GetResourceWorkDetails", method = RequestMethod.POST)
    public Response GetResourceWorkDetails(
            @ApiParam(value = "RequestDTO from UI", required = true) @RequestBody RequestDTO requestDTO) throws AuthorizationException {
        logger.info("Request Objects for get operations:  " + requestDTO);
        try {
            if (requestDTO.getChargeMonth() == null || requestDTO.getChargeMonth().equalsIgnoreCase("0")
                    || requestDTO.getChargeMonth().equalsIgnoreCase("")) {
                chargeMonthMaster = chargeMonthMasterService.findByChargeMonthForCurrentMonth();
            } else {
                chargeMonthMaster = chargeMonthMasterService.findByChargeMonthAndYear(requestDTO.getChargeMonth(),
                        Integer.valueOf(requestDTO.getChargeYear()));
            }

            if (Integer.valueOf(requestDTO.getUserRole()) <= 2) {
                List<_lstRSCWH> list = tblResourcechargemonthhoursService
                        .find_lstRSCWH(chargeMonthMaster.getchargemonthid());
                list.forEach(item -> item.setChargemonthid(chargeMonthMaster.getchargemonthid()));
                list.sort(Comparator.comparing(_lstRSCWH::getUserName));
                List<_lstRSCWH> esaList = tblEsaResourcechargemonthhoursService
                        .find_lstRSCWH(chargeMonthMaster.getchargemonthid());
                esaList.sort(Comparator.comparing(_lstRSCWH::getUserName));
                esaList.forEach(item -> item.setChargemonthid(chargeMonthMaster.getchargemonthid()));
                result = lSTRSCWHDeeremapper(list, result);
                result = lSTRSCWHESAmapper(esaList, result);
                result = resultMapper(chargeMonthMaster, result);
                response.setResult(result);

            } else if (Integer.valueOf(requestDTO.getUserRole()) == 4) {
                Date date = new Date();
                //if (date.after(chargeMonthMaster.getfromdate()) && date.before(chargeMonthMaster.gettodate())) {
                    List<_lstRSCWH> list = tblResourcechargemonthhoursService.find_lstRSCWHForDeereUser(chargeMonthMaster.getchargemonthid(), Integer.valueOf(requestDTO.getUserID()));
                    list.forEach(item -> item.setChargemonthid(chargeMonthMaster.getchargemonthid()));
                    List<_lstRSCWH> esaList = tblEsaResourcechargemonthhoursService.find_lstRSCWHForDeereUser(
                            chargeMonthMaster.getchargemonthid(), Integer.valueOf(requestDTO.getUserID()));
                    esaList.forEach(item -> item.setChargemonthid(chargeMonthMaster.getchargemonthid()));
                    result = lSTRSCWHDeeremapper(list, result);
                    result = lSTRSCWHESAmapper(esaList, result);
                    result = resultMapper(chargeMonthMaster, result);
                    response.setResult(result);
//                } else {
//                    List<LstRSCWH> emptyList = new ArrayList<>();
//                    List<LstESARSCWH>  emptylstESARSCWH =new ArrayList<>();
//                    result.setLstRSCWH(emptyList);
//                    result.setLstESARSCWH(emptylstESARSCWH);
//                    result = resultMapper(chargeMonthMaster, result);
//                    response.setResult(result);
//                }
            } else {
                List<_lstRSCWH> list = tblResourcechargemonthhoursService.find_lstRSCWHForUser(
                        chargeMonthMaster.getchargemonthid(), Integer.valueOf(requestDTO.getUserID()));
                list.sort(Comparator.comparing(_lstRSCWH::getUserName));
                list.forEach(item -> item.setChargemonthid(chargeMonthMaster.getchargemonthid()));
                List<_lstRSCWH> esaList = tblEsaResourcechargemonthhoursService.find_lstRSCWHForUser(
                        chargeMonthMaster.getchargemonthid(), Integer.valueOf(requestDTO.getUserID()));
                esaList.sort(Comparator.comparing(_lstRSCWH::getUserName));
                esaList.forEach(item -> item.setChargemonthid(chargeMonthMaster.getchargemonthid()));
                result = lSTRSCWHDeeremapper(list, result);
                result = lSTRSCWHESAmapper(esaList, result);
                result = resultMapper(chargeMonthMaster, result);
                response.setResult(result);
            }
            return response;
        }
        catch ( Exception e) {
            throw new DataNotFoundException("No Data found in the database for the given input" + " ChargeMonth="
                    + requestDTO.getChargeMonth() + " ChargeYear=" + requestDTO.getChargeYear() + " UserId="
                    + requestDTO.getUserID() + " UserRole=" + requestDTO.getUserRole());
        }
    }

    @CrossOrigin
    @ApiOperation(value = "update details of timesheet data")
    @RequestMapping(value = "/UpdateResourceWorkDetails", method = RequestMethod.PUT)
    public Boolean UpdateResourceWorkDetails(
            @ApiParam(value = "RequestForUpdate from UI", required = true) @RequestBody RequestForUpdate[] requestForUpdate) {
        for (RequestForUpdate requestArray : requestForUpdate) {
            logger.info("Request Objects For Update/Insert :  " + requestArray);
        }
        //    logger.info("Request Objects :  " + requestForUpdate[0]);
        try {

            int rowCount = tblResourcechargemonthhoursService.find_countForUserAndChargeMonthId(
                    requestForUpdate[0].getChargeMonthId(), requestForUpdate[0].getUserId());
            if (rowCount == 0) {
                TblResourcechargemonthhours tblResourcechargemonthhours = dataPopulator(requestForUpdate[0]);
                tblResourcechargemonthhoursService.save(tblResourcechargemonthhours);
            } else {
                tblResourcechargemonthhoursService.updateTimesheetData(requestForUpdate[0].getUserId(), requestForUpdate[0].getRate(),
                        requestForUpdate[0].getChargeMonthId(), requestForUpdate[0].getWeek1(),
                        requestForUpdate[0].getWeek2(), requestForUpdate[0].getWeek3(), requestForUpdate[0].getWeek4(),
                        requestForUpdate[0].getWeek5(), requestForUpdate[0].getWeek6(), requestForUpdate[0].getComments());
            }
            return true;
        } catch (Exception e) {
            logger.error("Null Data passed in the database for the given input" + " UserId="
                    + requestForUpdate[0].getUserId() + " ChargeMonthId=" + requestForUpdate[0].getChargeMonthId()
                    + " week1=" + requestForUpdate[0].getWeek1() + " week2=" + requestForUpdate[0].getWeek2()
                    + " week3=" + requestForUpdate[0].getWeek3() + " week4=" + requestForUpdate[0].getWeek4()
                    + " week5=" + requestForUpdate[0].getWeek5() + " week6=" + requestForUpdate[0].getWeek6() + " Comments=" + requestForUpdate[0].getComments());
            throw new DataNotFoundException("Null Data passed in the database for the given input" + " UserId="
                    + requestForUpdate[0].getUserId() + " ChargeMonthId=" + requestForUpdate[0].getChargeMonthId()
                    + " week1=" + requestForUpdate[0].getWeek1() + " week2=" + requestForUpdate[0].getWeek2()
                    + " week3=" + requestForUpdate[0].getWeek3() + " week4=" + requestForUpdate[0].getWeek4()
                    + " week5=" + requestForUpdate[0].getWeek5() + " week6=" + requestForUpdate[0].getWeek6() + " Comments=" + requestForUpdate[0].getComments());
        }
    }

    private Result lSTRSCWHDeeremapper(List<_lstRSCWH> list, Result result) {
        List<LstRSCWH> lstRSCWHList = new ArrayList<>();
        for (_lstRSCWH easData : list) {
            LstRSCWH lstRSCWH = new LstRSCWH();
            lstRSCWH.setChargeMonthId(easData.getChargemonthid());
            lstRSCWH.setComments(easData.getComments());
            /*
             * lstESARSCWH.setCostCenter(); lstESARSCWH.setCtsId(ctsId);
             * lstESARSCWH.setId(); lstESARSCWH.setName(name);
             * lstESARSCWH.setPoNumber(poNumber); lstESARSCWH.setProjectId(projectId);
             * lstESARSCWH.setProjectManager(projectManager);
             * lstESARSCWH.setProjectName(projectName);
             * lstESARSCWH.setProjectManager(projectManager);
             * lstESARSCWH.setProjectName(projectName); lstESARSCWH.setRacfid(racfid);
             * lstESARSCWH.setRate(rate);
             */
            // lstESARSCWH.setTotalHours(totalHours);
            lstRSCWH.setUserId(easData.getUserid());
            lstRSCWH.setUsername(easData.getUserName());
            if (easData.getWeek1() != null) {
                lstRSCWH.setWeek1(easData.getWeek1());
            } else {
                lstRSCWH.setWeek1(0);
            }
            if (easData.getWeek2() != null) {
                lstRSCWH.setWeek2(easData.getWeek2());
            } else {
                lstRSCWH.setWeek2(0);
            }
            if (easData.getWeek3() != null) {
                lstRSCWH.setWeek3(easData.getWeek3());
            } else {
                lstRSCWH.setWeek3(0);
            }
            if (easData.getWeek4() != null) {
                lstRSCWH.setWeek4(easData.getWeek4());
            } else {
                lstRSCWH.setWeek4(0);
            }
            if (easData.getWeek5() != null) {
                lstRSCWH.setWeek5(easData.getWeek5());
            } else {
                lstRSCWH.setWeek5(0);
            }
            if (easData.getWeek6() != null) {
                lstRSCWH.setWeek6(easData.getWeek6());
            } else {
                lstRSCWH.setWeek6(0);
            }

            //	System.out.println(easData.getRate());
            lstRSCWH.setRate(easData.getRate());
            lstRSCWHList.add(lstRSCWH);

        }

        result.setLstRSCWH(lstRSCWHList);
        return result;
    }

    private Result lSTRSCWHESAmapper(List<_lstRSCWH> esaList, Result result) {
        List<LstESARSCWH> lstESARSCWHList = new ArrayList<>();
        for (_lstRSCWH easData : esaList) {
            LstESARSCWH lstESARSCWH = new LstESARSCWH();
            lstESARSCWH.setChargeMonthId(easData.getChargemonthid());
            lstESARSCWH.setComments(easData.getComments());
            /*
             * lstESARSCWH.setCostCenter(); lstESARSCWH.setCtsId(ctsId);
             * lstESARSCWH.setId(); lstESARSCWH.setName(name);
             * lstESARSCWH.setPoNumber(poNumber); lstESARSCWH.setProjectId(projectId);
             * lstESARSCWH.setProjectManager(projectManager);
             * lstESARSCWH.setProjectName(projectName);
             * lstESARSCWH.setProjectManager(projectManager);
             * lstESARSCWH.setProjectName(projectName); lstESARSCWH.setRacfid(racfid);
             * lstESARSCWH.setRate(rate);
             */
            // lstESARSCWH.setTotalHours(totalHours);
            lstESARSCWH.setUserId(easData.getUserid());
            lstESARSCWH.setUsername(easData.getUserName());
            if (easData.getWeek1() != null) {
                lstESARSCWH.setWeek1(easData.getWeek1());
            } else {
                lstESARSCWH.setWeek1(0);
            }
            if (easData.getWeek2() != null) {
                lstESARSCWH.setWeek2(easData.getWeek2());
            } else {
                lstESARSCWH.setWeek2(0);
            }
            if (easData.getWeek3() != null) {
                lstESARSCWH.setWeek3(easData.getWeek3());
            } else {
                lstESARSCWH.setWeek3(0);
            }
            if (easData.getWeek4() != null) {
                lstESARSCWH.setWeek4(easData.getWeek4());
            } else {
                lstESARSCWH.setWeek4(0);
            }
            if (easData.getWeek5() != null) {
                lstESARSCWH.setWeek5(easData.getWeek5());
            } else {
                lstESARSCWH.setWeek5(0);
            }
            if (easData.getWeek6() != null) {
                lstESARSCWH.setWeek6(easData.getWeek6());
            } else {
                lstESARSCWH.setWeek6(0);
            }
            lstESARSCWH.setRate(easData.getRate());
            lstESARSCWHList.add(lstESARSCWH);
        }

        result.setLstESARSCWH(lstESARSCWHList);
        return result;
    }

    private Result resultMapper(TblChargemonthmaster chargeMonthMaster, Result result) {
        DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        result.setChargeYear(Integer.valueOf(chargeMonthMaster.getYear()));
        result.setBillingTo(dateFormat.format(chargeMonthMaster.gettodate()));
        result.setBillingFrom(dateFormat.format(chargeMonthMaster.getfromdate()));
        result.setChargeMonth(chargeMonthMaster.getchargemonth());
        result.setChargeMonthID(chargeMonthMaster.getchargemonthid());
        return result;
    }


    private TblResourcechargemonthhours dataPopulator(RequestForUpdate requestForUpdate) {
        TblResourcechargemonthhours tblResourcechargemonthhours = new TblResourcechargemonthhours();
        tblResourcechargemonthhours.setFkUserid(requestForUpdate.getUserId());
        tblResourcechargemonthhours.setFkChargemonthid(requestForUpdate.getChargeMonthId());
        tblResourcechargemonthhours.setWeek1(requestForUpdate.getWeek1());
        tblResourcechargemonthhours.setWeek2(requestForUpdate.getWeek2());
        tblResourcechargemonthhours.setWeek3(requestForUpdate.getWeek3());
        tblResourcechargemonthhours.setWeek4(requestForUpdate.getWeek4());
        tblResourcechargemonthhours.setWeek5(requestForUpdate.getWeek5());
        tblResourcechargemonthhours.setWeek6(requestForUpdate.getWeek6());
        tblResourcechargemonthhours.setComments(requestForUpdate.getComments());
        return tblResourcechargemonthhours;
    }

    @ExceptionHandler(DataNotFoundException.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
        ErrorResponse error = new ErrorResponse();
        error.setErrorCode(HttpStatus.BAD_REQUEST.value());
        error.setMessage(ex.getMessage());
        return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
    }

    @ExceptionHandler(AuthorizationException.class)
    public ResponseEntity<ErrorResponse> authExceptionHandler(Exception ex) {
        ErrorResponse error = new ErrorResponse();
        error.setErrorCode(HttpStatus.FORBIDDEN.value());
        error.setMessage(ex.getMessage());
        return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
    }

}
