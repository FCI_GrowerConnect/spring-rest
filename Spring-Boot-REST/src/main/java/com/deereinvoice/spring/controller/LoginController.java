package com.deereinvoice.spring.controller;

import com.deereinvoice.spring.dto.LoginResponse;
import com.deereinvoice.spring.dto.LoginResult;
import com.deereinvoice.spring.dto.UserModel;
import com.deereinvoice.spring.exception.DataNotFoundException;
import com.deereinvoice.spring.exception.ErrorResponse;
import com.deereinvoice.spring.model.TblUserdetails;
import com.deereinvoice.spring.service.UserDetailsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/User")
@Api(value = "Deere Invoice Management System", description = "Login Management")
public class LoginController {

    Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    UserDetailsService userDetailsService;

    private TblUserdetails tb1Userdetails;

    @Autowired
    private LoginResponse loginResponse;

    @Autowired
    private LoginResult loginResult;

    @CrossOrigin
    @ApiOperation(value = "UserDetails ")
    @RequestMapping(value = "/UserDetails", method = RequestMethod.POST)
    public LoginResult GetResourceWorkDetails(
            @ApiParam(value = "RequestDTO from UI", required = false) @RequestBody UserModel userModel) {
        try {
            logger.info("Request with username=" + userModel.getUsername());
            tb1Userdetails = userDetailsService.getUserByUserId(Integer.parseInt(userModel.getUsername()));
            loginResult.setResult(setReponseForLogin(tb1Userdetails));
            return loginResult;
        } catch (Exception e) {
            logger.error("No User found in the database for" + " Username="
                    + userModel.getUsername());
            throw new DataNotFoundException("No User found in the database for the " + " Username="
                    + userModel.getUsername());

        }
    }

    private LoginResponse setReponseForLogin(TblUserdetails tb1Userdetails) {
        loginResponse.set_loginName(tb1Userdetails.getUsername());
        loginResponse.set_userId(tb1Userdetails.getUserid());
        if (tb1Userdetails.getfRoleid() != null) {
            loginResponse.set_userRole(tb1Userdetails.getfRoleid());
        } else {
            loginResponse.set_userRole(4);
        }
        loginResponse.set_token("12345");
        return loginResponse;
    }

    @ExceptionHandler(DataNotFoundException.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
        ErrorResponse error = new ErrorResponse();
        error.setErrorCode(HttpStatus.NOT_FOUND.value());
        error.setMessage(ex.getMessage());
        return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
    }
}
