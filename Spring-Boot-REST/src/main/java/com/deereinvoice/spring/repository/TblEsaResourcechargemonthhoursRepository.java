package com.deereinvoice.spring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.deereinvoice.spring.dto._lstRSCWH;
import com.deereinvoice.spring.model.TblEsaResourcechargemonthhours;

public interface TblEsaResourcechargemonthhoursRepository
		extends JpaRepository<TblEsaResourcechargemonthhours, String> {

	@Query("SELECT new com.deereinvoice.spring.dto._lstRSCWH(UD.userid,UD.username,UD.racfid,14 ,RCM.week1,RCM.week2,RCM.week3,RCM.week4,RCM.week5,RCM.week6,RCM.comments,RCM.rate)"
			+ "FROM TblEsaResourcechargemonthhours AS RCM"
			+ " RIGHT JOIN TblUserdetails AS UD ON UD.userid=RCM.fkUserid AND RCM.fkChargemonthid=:chargemonthid")
	public List<_lstRSCWH> find_lstRSCWH(@Param("chargemonthid") int chargemonthid);

	@Query("SELECT new com.deereinvoice.spring.dto._lstRSCWH(UD.userid,UD.username,UD.racfid,14 ,RCM.week1,RCM.week2,RCM.week3,RCM.week4,RCM.week5,RCM.week6,RCM.comments,RCM.rate)"
			+ "FROM TblEsaResourcechargemonthhours AS RCM"
			+ " RIGHT JOIN TblUserdetails AS UD ON UD.userid=RCM.fkUserid AND RCM.fkChargemonthid=:chargemonthid WHERE UD.userid IN(SELECT DISTINCT(fkUserid)AS FK_UserID from TblEsatimesheetdata where projectManagerid=:userid)")
	public List<_lstRSCWH> find_lstRSCWHForUser(@Param("chargemonthid") int chargemonthid, @Param("userid") int userid);

	@Query("SELECT new com.deereinvoice.spring.dto._lstRSCWH(UD.userid,UD.username,UD.racfid,14 ,RCM.week1,RCM.week2,RCM.week3,RCM.week4,RCM.week5,RCM.week6,RCM.comments,RCM.rate)"
			+ "FROM TblEsaResourcechargemonthhours AS RCM"
			+ " RIGHT JOIN TblUserdetails AS UD ON UD.userid=RCM.fkUserid AND RCM.fkChargemonthid=:chargemonthid WHERE UD.userid IN(SELECT DISTINCT(fkUserid)AS FK_UserID from TblEsatimesheetdata where fkUserid=:userid)")
    public List<_lstRSCWH> find_lstRSCWHForDeereUser(@Param("chargemonthid")int chargemonthid,  @Param("userid") Integer userid);
}
