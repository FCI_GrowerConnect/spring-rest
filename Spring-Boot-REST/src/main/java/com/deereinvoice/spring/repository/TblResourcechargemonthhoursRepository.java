package com.deereinvoice.spring.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.deereinvoice.spring.dto._lstRSCWH;
import com.deereinvoice.spring.model.TblResourcechargemonthhours;

public interface TblResourcechargemonthhoursRepository extends JpaRepository<TblResourcechargemonthhours, String> {

	@Query("SELECT new com.deereinvoice.spring.dto._lstRSCWH(UD.userid,UD.username,UD.racfid,17,RCM.week1,RCM.week2,RCM.week3,RCM.week4,RCM.week5,RCM.week6,RCM.comments,RCM.rate)"
			+ "FROM TblResourcechargemonthhours AS RCM"
			+ " RIGHT JOIN TblUserdetails AS UD ON UD.userid=RCM.fkUserid AND RCM.fkChargemonthid=:chargemonthid")
	public List<_lstRSCWH> find_lstRSCWH(@Param("chargemonthid") int chargemonthid);

	@Query("SELECT new com.deereinvoice.spring.dto._lstRSCWH(UD.userid,UD.username,UD.racfid,17 ,RCM.week1,RCM.week2,RCM.week3,RCM.week4,RCM.week5,RCM.week6,RCM.comments,RCM.rate)"
			+ "FROM TblResourcechargemonthhours AS RCM"
			+ " RIGHT JOIN TblUserdetails AS UD ON UD.userid=RCM.fkUserid AND RCM.fkChargemonthid=:chargemonthid WHERE UD.userid IN(SELECT DISTINCT(fkUserid)AS FK_UserID from TblEsatimesheetdata where projectManagerid=:userid)")
	public List<_lstRSCWH> find_lstRSCWHForUser(@Param("chargemonthid") int chargemonthid, @Param("userid") int userid);

	@Query("SELECT COUNT(srid) FROM TblResourcechargemonthhours WHERE fkUserid=:userid AND fkChargemonthid=:chargemonthid")
	public Integer find_countForUserAndChargeMonthId(@Param("chargemonthid") int chargemonthid,
			@Param("userid") int userid);

	@Query("SELECT max(srid) FROM TblResourcechargemonthhours")
	public Integer find_maxSrId();

	@Transactional
	@Modifying
	@Query("UPDATE TblResourcechargemonthhours set fkUserid = :userid, rate = :rate ,fkChargemonthid=:chargemonthid,week1=:week1,week2=:week2,week3=:week3,week4=:week4,week5=:week5,week6=:week6,comments=:comments WHERE fkUserid=:userid AND fkChargemonthid=:chargemonthid")
	public void updateTimesheetData(@Param("userid") int userid, @Param("rate") int rate ,@Param("chargemonthid") int chargemonthid,
			@Param("week1") int week1, @Param("week2") int week2, @Param("week3") int week3, @Param("week4") int week4,
			@Param("week5") int week5, @Param("week6") int week6, @Param("comments") String comments);


	@Query("SELECT new com.deereinvoice.spring.dto._lstRSCWH(UD.userid,UD.username,UD.racfid,17 ,RCM.week1,RCM.week2,RCM.week3,RCM.week4,RCM.week5,RCM.week6,RCM.comments,RCM.rate)"
			+ "FROM TblResourcechargemonthhours AS RCM"
			+ " RIGHT JOIN TblUserdetails AS UD ON UD.userid=RCM.fkUserid AND RCM.fkChargemonthid=:chargemonthid WHERE UD.userid IN(SELECT DISTINCT(fkUserid)AS FK_UserID from TblEsatimesheetdata where fkUserid=:userid)")
   public List<_lstRSCWH> find_lstRSCWHFOrDeereuser(@Param("chargemonthid") int chargemonthid, @Param("userid") int userid);
}
