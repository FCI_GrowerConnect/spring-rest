package com.deereinvoice.spring.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.deereinvoice.spring.model.TblChargemonthmaster;

public interface ChargeMonthMasterRepository extends CrudRepository<TblChargemonthmaster, Integer> {

	@Query("SELECT t FROM TblChargemonthmaster t WHERE t.chargemonth LIKE :test AND t.year = :year")
	public TblChargemonthmaster findByChargeMonthAndYear(@Param("test") String test, @Param("year") int year);

	@Query("SELECT t FROM TblChargemonthmaster t WHERE GETDATE() BETWEEN fromdate AND todate")
	public TblChargemonthmaster findByChargeMonthForCurrentMonth();
}
