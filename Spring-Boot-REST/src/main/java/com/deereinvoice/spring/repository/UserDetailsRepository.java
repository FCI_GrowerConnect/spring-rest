package com.deereinvoice.spring.repository;

import com.deereinvoice.spring.model.TblUserdetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;



public interface UserDetailsRepository extends JpaRepository<TblUserdetails, Integer> {

    @Query(value="SELECT * FROM tbl_UserDetails  where tbl_UserDetails.userid=:userid",nativeQuery = true)
    public TblUserdetails findByUserId(@Param("userid") int userid);
}
